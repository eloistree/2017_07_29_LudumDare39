﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class KeyToWordDetection : MonoBehaviour {

    public KeyListener _keyListener;
    [System.Serializable]
    public class OnWordDetectedUnityEvent : UnityEvent<string>{ }
    public OnWordDetectedUnityEvent _onWordDetected;
    public string[] _listenedWords= new string[] { "HeyMonAmi"};
    public StringComparison _caseSensitive = StringComparison.CurrentCultureIgnoreCase;

    public string _current;

    void Start() {
        if (_keyListener == null)
            _keyListener = KeyListener.Instance;
        if (_keyListener != null)
            _keyListener._onKeyDown.AddListener(CheckForWord);

    }

    private void CheckForWord(char characterPressed, KeyListener.KeyInputEvent keyPressed)
    {
        _current += characterPressed;

        string wordFound = null;

        for (int i  = 0; i  < _listenedWords.Length ; i ++)
        {
            string word = _listenedWords[i];
            int index = _current.LastIndexOf(word, _caseSensitive);
            if (index>-1 && (index + word.Length == _current.Length))
            {
                wordFound = word;
            }

        }


        if (!string.IsNullOrEmpty( wordFound ) )
        {
            _current = "";
            //Debug.Log("uu " +wordFound);
            _onWordDetected.Invoke(wordFound);

        }
    }

    void Update () {
		
	}
}
