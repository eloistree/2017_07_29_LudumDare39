﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class KeyListener : MonoBehaviour {

    public static KeyListener _instance;
    public static KeyListener Instance { get {

            if (_instance != null) return _instance;
            _instance = FindObjectOfType<KeyListener>();
            if (_instance != null) return _instance;
            GameObject instanceCreated = new GameObject("# Key Listener Manager");
            _instance = instanceCreated.AddComponent<KeyListener>();
            AddDefaultBehaviour();
            return _instance;
        }
        set { _instance = value; AddDefaultBehaviour();  }
    }

    public static void AddDefaultBehaviour()
    {
        if (FindObjectOfType<KeyInputDefault>() == null)
            Instance.gameObject.AddComponent<KeyInputDefault>();
        if (FindObjectOfType<DefaultAutoFlush>() == null)
            Instance.gameObject.AddComponent<DefaultAutoFlush>(); 
    }

    void Awake()
    {
        Instance = this;
    }

    [System.Serializable]
    public class UnitCharEvent : UnityEvent<char, KeyInputEvent> { };
    public UnitCharEvent _onKeyDown;
    public UnitCharEvent _onKeyUp;

    public void FlushAllbutLast(int count) {

        if(_inputHistory.Count >= count)
            _inputHistory = _inputHistory.GetRange(_inputHistory.Count- count, count);
    }
    


    public List<KeyInputEvent> _inputHistory = new List<KeyInputEvent>();
    public char[] GetLastKeysAsChar(int number = -1, KeyInputEvent.Type type = KeyInputEvent.Type.Down)
    {
        char[] chars = _inputHistory.Where(x => x._type == KeyInputEvent.Type.Down).OrderByDescending(x => x._time).Select(x => x._input).ToArray();
        if (number < 1)
            chars = chars.Take(number).ToArray();
        return chars;
    }

    public string GetLastKeys(int number = -1, KeyInputEvent.Type type = KeyInputEvent.Type.Down)
    {
        return new string(GetLastKeysAsChar(number, type));
    }


    public void AddInput(string input, float when, KeyInputEvent.Type type)
    {
        for (int i = 0; i < input.Length; i++)
        {
            AddInput(input[i], when, type);
        }
    }
    public void AddInput(KeyCode input, float when, KeyInputEvent.Type type)
    {
        AddInput((char) input, when, type);
    }
    public void AddInput(char input, float when, KeyInputEvent.Type type)
    {
        KeyInputEvent key = new KeyInputEvent(input, when, type);
        _inputHistory.Add(key);
        if (type == KeyInputEvent.Type.Down)
            _onKeyDown.Invoke(input, key);
        if (type == KeyInputEvent.Type.Up)
            _onKeyUp.Invoke(input, key);
    }

    public void Flush() {
        _inputHistory.Clear();
    }

    [System.Serializable]
    public struct KeyInputEvent {

        public enum Type { Down, Up }
        public char _input;
        public Type _type;
        public float _time;
        public KeyInputEvent(KeyCode input, float when, Type type) : this((char) input, when, type)
        {

        }
        public KeyInputEvent(char input, float when, Type type) 
        {
            if (when <= 0f) throw new System.ArgumentException("When action is gone must be define.");
            _type = type;
            _time = when;
            _input = input;
        }
        
        
    }
    
}



public class KeyInputDefault : MonoBehaviour
{

    public string _previousInput="";
    public string _currentInput="";

    public void Update()
    {
        CheckForKeyUpAndDown();
    }

    public void CheckForKeyUpAndDown()
    {
        _currentInput = Input.inputString;

        foreach (char c in _previousInput.ToArray())
        {
            if (!_currentInput.Contains(c))
                KeyListener.Instance.AddInput(c, Time.realtimeSinceStartup, KeyListener.KeyInputEvent.Type.Up);
        }
        foreach (char c in _currentInput.ToArray())
        {
            if (!_previousInput.Contains(c))
                KeyListener.Instance.AddInput(c, Time.realtimeSinceStartup, KeyListener.KeyInputEvent.Type.Down);
        }
        //if(_currentInput!=""  || _previousInput!="")
        // Debug.Log("Current:" + _currentInput + " >><< " + _previousInput);
        //foreach (KeyCode keyCode in System.Enum.GetValues(typeof(KeyCode)))
        //{
        //    if ( Input.GetKeyUp(keyCode) )
        //        KeyListener.Instance.AddInput(keyCode, Time.realtimeSinceStartup, KeyListener.KeyInputEvent.Type.Up);
        //    if ( Input.GetKeyDown(keyCode) )
        //        KeyListener.Instance.AddInput(keyCode, Time.realtimeSinceStartup, KeyListener.KeyInputEvent.Type.Down);
        //}
        _previousInput = _currentInput;
    }
}
public class DefaultAutoFlush : MonoBehaviour
{
    public int _maxRecord = 200;

    void Awake()
    {
        InvokeRepeating("CheckToFlush", 0, 0.5f);
    }

    public void CheckToFlush()
    {
        KeyListener.Instance.FlushAllbutLast(_maxRecord);
    }
}