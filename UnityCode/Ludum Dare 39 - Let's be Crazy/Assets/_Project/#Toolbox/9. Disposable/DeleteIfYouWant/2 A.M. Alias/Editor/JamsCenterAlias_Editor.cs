﻿
using UnityEditor;
using UnityEngine;

namespace J2AM.Toolbox.EditorScripts
{ 
public class JamsCenterAlias_Editor : Editor
{
    
        [MenuItem("2 A.M./Links/Website/2 A.M. Patreon")]
        public static void OpenWebsitePage_2AM()
        {

            Application.OpenURL("http://jams.center/2AM/");
        }
        [MenuItem("2 A.M./Links/Website/Jams.Center")]
        public static void OpenWebsitePage_JamsCenter()
        {

            Application.OpenURL("http://jams.center/");
        }
        
        [MenuItem("2 A.M./Links/Downloadable/Models")]
        public static void OpenWebsitePage_Download_3D()
        {
            Application.OpenURL("https://sketchfab.com/JamsCenterStudio");
        }
        [MenuItem("2 A.M./Links/Downloadable/Quick Script")]
        public static void OpenWebsitePage_Download_QuickScript()
        {
            Application.OpenURL("https://github.com/JamsCenter/Toolbox/tree/master/QuickScript");
        }
        [MenuItem("2 A.M./Links/Downloadable/Toolbox")]
        public static void OpenWebsitePage_Download_Toolbox()
        {
            Application.OpenURL("https://github.com/JamsCenter/Toolbox/");
        }


        [MenuItem("2 A.M./Links/Quick Access/♪ Coding Music ♫")]
        public static void OpenWebsitePage_QuickAcces_Music()
        {
            Application.OpenURL("https://www.youtube.com/playlist?list=PLfOULzCct2SKQ-lRgO5Wp2SkZ90EyRiPo");

        }

        [MenuItem("2 A.M./Links/Quick Access/Google Search")]
        public static void OpenWebsitePage_QuickAcces_Google()
        {
            Application.OpenURL("https://www.google.com/");


        }

        [MenuItem("2 A.M./Links/Quick Access/Google Tranlsate")]
        public static void OpenWebsitePage_QuickAcces_GoogleTranslate()
        {
            Application.OpenURL("https://translate.google.com/");

        }



    }
}