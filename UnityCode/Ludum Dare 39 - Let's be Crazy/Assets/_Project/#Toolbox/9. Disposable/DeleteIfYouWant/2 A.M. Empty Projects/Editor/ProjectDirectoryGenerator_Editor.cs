﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class ProjectDirectoryGenerator_Editor
{


    [MenuItem("2 A.M./Toolbox/Create/Directory/Empty Project Directories")]
    static void GenerateProjectStrucutre()
    {
        string projectPath = Application.dataPath;
        Directory.CreateDirectory(projectPath + "/_Project");
        Directory.CreateDirectory(projectPath + "/_Project/Assets");
        Directory.CreateDirectory(projectPath + "/_Project/Assets/3D");
        Directory.CreateDirectory(projectPath + "/_Project/Assets/2D");
        Directory.CreateDirectory(projectPath + "/_Project/Material");
        Directory.CreateDirectory(projectPath + "/_Project/Prefab/");
        Directory.CreateDirectory(projectPath + "/_Project/Scene/");
        CreateShortCut(projectPath + "/_Project/", "Download_Empty_Project", "https://github.com/JamsCenter/2017_02_05_UnityEmptyProject/archive/master.zip");
        CreateShortCut(projectPath + "/_Project/", "Access_Empty_Project", "https://github.com/JamsCenter/2017_02_05_UnityEmptyProject/");
        Application.OpenURL("https://github.com/JamsCenter/2017_02_05_UnityEmptyProject/");

    }

    
    [MenuItem("2 A.M./Toolbox/Create/Directory/OSig Directory")]
    static void GenerateProjectOsigFolder()
    {
        string projectPath = Application.dataPath;
        Directory.CreateDirectory(projectPath + "/Plugins");
        Directory.CreateDirectory(projectPath + "/Plugins/Android");
        Directory.CreateDirectory(projectPath + "/Plugins/Android/assets");
        CreateShortCut(projectPath + "/Plugins/Android/assets/", "Manual_OSig", "https://developer.oculus.com/documentation/mobilesdk/latest/concepts/mobile-submission-sig-file/#mobile-submission-sig-file");
        CreateShortCut(projectPath + "/Plugins/Android/assets/", "Generate_OSig", "https://dashboard.oculus.com/tools/osig-generator/");
        CreateShortCut(projectPath + "/Plugins/Android/assets/", "Workshop_OSig", "https://github.com/JamsCenter/2017_06_15_HelloVirtualReality/wiki/Access-OSig-File-for-Gear-VR");
    }

    public static void CreateShortCut(string directoryPath,string fileName, string shotcutUrl) {
        string shortcutContent = "[InternetShortcut]\nURL = " + shotcutUrl;
        File.WriteAllText(directoryPath  + fileName + ".url", shortcutContent);


    }
}