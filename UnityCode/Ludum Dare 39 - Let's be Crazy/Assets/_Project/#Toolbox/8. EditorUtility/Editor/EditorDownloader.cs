﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;

public class EditorDownloader {

    //WIP NOT TESTED YET
    public static void Load(string url, string fileName)
    {
        using (WebClient client = new WebClient())
        {
            client.DownloadFile(url, fileName);
        }
    }
    //WIP NOT TESTED YET
    public static void Load(string url, string filePath, System.Action<string> fileContentLoaded)
    {
        Load(url, filePath);
        if (File.Exists(filePath))
        {
            fileContentLoaded.Invoke(File.ReadAllText(filePath));
        }
    }
    //WIP NOT TESTED YET
    public static void Load(string url, string filePath, System.Action<Texture> textureLoaded)
    {
        Texture2D text = null;
        Load(url, filePath);
        if (File.Exists(filePath))
        {
            byte [] fileBytesContent = File.ReadAllBytes(filePath);
            text = new Texture2D(1, 1);
            text.LoadImage(fileBytesContent);
        }
        textureLoaded.Invoke(text);

    }
    
}
