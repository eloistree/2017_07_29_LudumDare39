﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveForward : MonoBehaviour {

    public Transform m_affectedObject;
    public float m_speed=5f;
	void Update () {
        if (m_affectedObject == null) return;
        m_affectedObject.position+=(m_affectedObject.forward*Time.deltaTime*m_speed);
		
	}
}
