﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerHeadDirection : MonoBehaviour 
{
	static public PlayerHeadDirection InstanceInScene;
	#region Public Members
	[Tooltip("Direction of the player head")]
	public Transform _cameraOrientation;
	[Tooltip("Direction of the eyees direction")]
	public Transform _playerDirection;
	[Header("Debug (Do not touch)")]
	public float _horizontalAngle;
	public float _verticalAngle;
	#endregion
	
	void Awake()
	{
        InstanceInScene = this;
	}

	void Update()
    {
        DefineAngles();
    }


    #region Main Methods

    private void DefineAngles()
    {

        Vector3 playerForward = _cameraOrientation.InverseTransformDirection(_playerDirection.forward);
        Vector3 directionHorizontal = new Vector3(playerForward.x, 0, playerForward.z);
        Vector3 directionVertical = new Vector3(0,playerForward.y, playerForward.z);
        _horizontalAngle = Vector3.Angle(Vector3.forward, directionHorizontal) * Mathf.Sign(playerForward.x);
        _verticalAngle = Vector3.Angle(Vector3.forward, directionVertical) * Mathf.Sign(playerForward.y);
        Debug.DrawLine(Vector3.zero, playerForward * 10f, Color.green);
       


    }
    
    #endregion

    #region Tools
    #endregion

    #region Private and Protected Members
    #endregion
}