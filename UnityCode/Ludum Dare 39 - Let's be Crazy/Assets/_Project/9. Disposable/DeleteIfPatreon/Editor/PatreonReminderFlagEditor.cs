﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
namespace J2AM.PatreonAPI.EditorScripts
{
    [InitializeOnLoad]

    public class PatreonReminderFlagEditor : Editor
    {
        //// http://jams.center/2AM/License/
        public static bool weReadAndUnderstandTheLicence = false;
        
        static PatreonReminderFlagEditor()
        {
            
            if (weReadAndUnderstandTheLicence)
                return;

            string date = DateTime.Now.ToShortDateString();
            string lastDiplay = PlayerPrefs.GetString("LastDisplay");
            
            if ( ! lastDiplay.Equals( date))
            {
                PlayerPrefs.SetString("LastDisplay", date );
                Debug.LogWarning("You are using the code of 2 A.M. / Jams.Center :) Thanks you ! " +
                    "\nPlease consider contributing if my works are useful to your team http://patreon.com/2AM" +
                    "\nTo remove this watermark, read the donation license: http://jams.center/2AM/License/" +
                    "\nAnd set the   'weReadAndUnderstandTheLicence'   to true   or   delete this file" +
                    "\n\n\n\n\n");

            }
        }
    }
}