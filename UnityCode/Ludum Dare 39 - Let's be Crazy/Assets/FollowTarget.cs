﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour {

    public Transform _affected;
    public Transform _targetToFollow;
    public float _movingSpeed=10f;
    public float _movingRotation= 2f;



    public Quaternion _wantedDirection;

	void Update () {

        Vector3 relativePos = _targetToFollow.position - _affected.position;
        _wantedDirection = Quaternion.LookRotation(relativePos);
        Quaternion rotation = Quaternion.Lerp(transform.rotation, _wantedDirection, Time.deltaTime*_movingRotation);

        _affected.rotation = rotation;
        _affected.position += _affected.forward *  _movingSpeed * Time.deltaTime; 
		
	}
}
