﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayerBasedOnHeadRotation : MonoBehaviour {

    public Transform _affected;
    public PlayerHeadDirection _headDirection;
    

    public PlayerHeadDirection HeadDirection
    {
        get {
            if (_headDirection == null)
                _headDirection = PlayerHeadDirection.InstanceInScene;
            return _headDirection;
        }
        
    }

    public float _horizontalRange=5;
    public float _verticalRange=2;


    public float _movingSpeed = 10f;

    public Vector3 _wantedLocalPosition;


    public float ClampAngle(float angle) { return Mathf.Clamp(angle, -90f, 90f)/90f; }

	void Update () {


        _wantedLocalPosition = new Vector3(ClampAngle(HeadDirection._horizontalAngle) * _horizontalRange , ClampAngle(HeadDirection._verticalAngle) * _verticalRange,0);

        _affected.localPosition = Vector3.Lerp(_affected.localPosition, _wantedLocalPosition, Time.deltaTime * _movingSpeed);



		
	}

    public void OnValidate()
    {
        if (_headDirection == null)
            _headDirection = FindObjectOfType<PlayerHeadDirection>();
    }
}
