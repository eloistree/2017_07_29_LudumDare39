﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateRandomPrefab : MonoBehaviour {


    public GameObject _prefabToGenerate;
    public Transform _directionRefPoint;
    public SpawnInformation _spawnInformation;

    [System.Serializable]
    public class SpawnInformation {
        public float _minTime=1f;
        public float _maxTime= 10f;

        public float _spawningRange = 50f;
        public float _spawningAngleRange = 180f;

    }

    public IEnumerator Start()
    {
        while (true) {
            yield return new WaitForSeconds(GetNextSpawnInSeconds());
            GeneratePrefab();
        }
    }

    private float GetNextSpawnInSeconds()
    {
        return GetRandomRange(_spawnInformation._minTime, _spawnInformation._maxTime);
    }

    void GeneratePrefab() {
        if (_prefabToGenerate == null || _directionRefPoint == null)
            return;
        GameObject created =   Instantiate(_prefabToGenerate, GetRandomPosition(), GetRandomRotation());
        //TODO Temporaire to clean
        created.name = "Random Prefab";
		
	}

    private Quaternion GetRandomRotation()
    {
        float angleRange = _spawnInformation._spawningAngleRange;
        Quaternion direction = _directionRefPoint.rotation;
        direction = Quaternion.Euler(GetRandomRange(angleRange), GetRandomRange(angleRange), 0)*direction;
        return direction;
    }

    private Vector3 GetRandomPosition()
    {
        float range = _spawnInformation._spawningRange;
        return _directionRefPoint.position + new Vector3 (GetRandomRange(range), GetRandomRange(range), GetRandomRange(range));
    }

    private float GetRandomRange(float range)
    {
        return UnityEngine.Random.Range(-range, range);
    }
    
        private float GetRandomRange(float min, float max )
    {
        if (max < min) {
            float tmp = min;
            min = max;
            max = tmp;
        }
        return UnityEngine.Random.Range(min, max);
    }
}
