﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class DispalyKeyboardInput : MonoBehaviour {

    public string _typedKeys;
    public Text _tDisplayTyped;


    public UnityEvent _onKeyInputDown;

    public List<string> _wordListened = new List<string>() { "missiles", "shield", "fire", "emp", "laser", "hyper-espace","teleport","speed up","slow down" };
    public UnityEvent _onListenedWordDetexted;



    #region  ACCESS FUNCTION
    public void FlushTyped() {
        _typedKeys = "";
        RefreshView();
    }

    #endregion

    void Update () {
        if (Input.anyKeyDown) {
            AddNeedKeyInput(Input.inputString.ToCharArray());
        }
        if (Input.GetKeyDown(KeyCode.Return))
            FlushTyped();
		
	}

    private void AddNeedKeyInput(char[] key)
    {
        for (int i = 0; i < key.Length; i++)
        {
            _typedKeys += key[i];
           
        }
        RefreshView();
    }

    private void RefreshView()
    {
        if (_tDisplayTyped)
        {
            _tDisplayTyped.text ="> "+ _typedKeys; }
    }
}
